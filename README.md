# Postulante Matias Navarro Desafío Transvip



## Desafío: 

Crear una aplicación que permita geolocalizar la ubicación del dispositivo registrando el historial de ubicaciones cada N segundos y persistiendo el registro en una base de datos interna del celular.


# Requerimientos Funcionales

-La app debe mostrar el mapa en donde esta ubicado.<br>
-En la mitad de la pantalla la app debe mostrar una lista de las ubicaciones almacenadas en la base de datos interna.<br>
-La app debe registrar la ubicación en segundo plano.<br>
-Cada registro de ubicación debe tener latitud, longitud y fecha de registro.<br>
-Los N segundos se especifica en el codigo a criterio del desarrollador.

## Requerimiento No Funcionales
- La aplicación que se vea bien con los widget en pantallas pequeñas desde 5.0 pulgadas evitando el famoso error de Warning por píxeles sobrepasados.  
- El proyecto flutter debe estar apuntando hacia Android 10 o superior.  
- Utilizar el patrón de diseño que se sienta más cómodo para el desarrollo y mencionarlo.  

